/*********************************************************
 * From C PROGRAMMING: A MODERN APPROACH, Second Edition *
 * By K. N. King                                         *
 * Copyright (c) 2008, 1996 W. W. Norton & Company, Inc. *
 * All rights reserved.                                  *
 * This program may be freely distributed for class use, *
 * provided that this copyright notice is retained.      *
 *********************************************************/

/* quadratic.c (Chapter 27, page 723) */
/* Finds the roots of the equation 5x**2 + 2x + 1 = 0 */

#include <complex.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(void)
{
  double a = 5, b = 2, c = 1;
  printf("Please Enter the value of a,b,c: ");
  scanf("%lf,%lf,%lf",&a,&b,&c);
  double complex discriminant_sqrt = csqrt(b * b - 4 * a * c);
  double complex root1 = (-b + discriminant_sqrt) / (2 * a);
  double complex root2 = (-b - discriminant_sqrt) / (2 * a);
  double x,y;
  x=cimag(root1);
  y=cimag(root2);
  printf("\n");
  if(x>=0&&y>0){
  printf("root1 = %.2lf + %.2lfi\n", creal(root1), fabs(x));
  printf("root2 = %.2lf + %.2lfi\n", creal(root2), fabs(y));
  
  }
  else if(x<0&&y>0){
  printf("root1 = %.2lf - %.2lfi\n", creal(root1), fabs(x));
  printf("root2 = %.2lf + %.2lfi\n", creal(root2), fabs(y));
  }
  else if(x>=0&&y<=0){
  printf("root1 = %.2lf + %.2lfi\n", creal(root1), fabs(x));
  printf("root2 = %.2lf - %.2lfi\n", creal(root2), fabs(y));
  }
  else if(x<0&&y<=0){
  printf("root1 = %.2lf - %.2lfi\n", creal(root1), fabs(x));
  printf("root2 = %.2lf - %.2lfi\n", creal(root2), fabs(y));
  }
  return 0;
}
