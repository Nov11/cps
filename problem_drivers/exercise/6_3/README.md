# 6_3 化简分数

## 题目

编写程序，要求用户输入一个分数，然后将其约分为最简分式：

    Enter a fraction: 6/12
    In lowest terms: 1/2

## 样例

### 样例一

    Enter a fraction: 6/12
    In lowest terms: 1/2

### 样例二

    Enter a fraction: 9/27
    In lowest terms: 1/3

## 数据范围

设输入的分子为 `n` ，输入的分母为 `m`，则 m ≠ 0， n < m 。

## 提示

1. 为了把分数约分为最简分式，首先计算分子和分母的最大公约数，然后分子和分母除以最大公约数。

2. 按照题目内容中的输出为规范格式进行输出。