# 26_2 调用函数 Invoke function

## 题目

编写程序写实 atexit 函数。除 main 函数外，程序还应包含两个函数。一个函数显示 That's all,，另一个显示 folk!。用 atexit 函数来注册这两个函数，使其可以再程序终止时被调用。清一定确保这两个函数按照正确的顺序进行调用，从而可以再屏幕上看到 That's all,folk!。

## 样例

That's all,folk!

## 数据范围

按照题目的标准进行输出。