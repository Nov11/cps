# 5_7 找出四个数中的最小值

## 题目

编写一个程序，从用户输入的4个整数中找出最大值和最小值：

    Enter four integers: 21 43 10 35
    Largest: 43
    Smallest: 10

## 样例

### 样例一

    Enter four integers: 21 43 10 35
    Largest: 43
    Smallest: 10

### 样例二

    Enter four integers: 1 2 3 4
    Largest: 4
    Smallest: 1

## 数据范围

可以假设输入的收入为 `n` ，则范围为整数。

## 提示

1. 尽可能少的用if语句。4条if语句足够了

2. 按照题目内容中的输出为规范格式进行输出。