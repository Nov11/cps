# 13_7 显示数字的英文单词 Display the word of number

## 题目

改进第 5 章的编程题 11，用字符串指针数组取代 switch 语句。例如，现在不再用 switch 语句来显示
第一位数字对应的单词，而把该数用作下标从包含 “twenty”、“thirty” 等字符串的数组中搜索。

## 样例

### 样例一

    Enter a two-gigit number: 45
    You entered the number forty-five.

### 样例二

    Enter a two-gigit number: 11
    You entered the number eleven.

## 数据范围

输入的数为 11 到 99。