#include<stdio.h>
#include<stddef.h>
struct s {
	char a;
	int b[2];
	float c;
}test;

int main(int argc, char *argv[])
{

	printf("the size of a: %d,  the offset of a: %d\n", (int)sizeof(test.a), (int)offsetof(struct s, a));
	printf("the size of b: %d,  the offset of b: %d\n", (int)sizeof(test.b), (int)offsetof(struct s, b));
	printf("the size of c: %d,  the offset of c: %d\n", (int)sizeof(test.c), (int)offsetof(struct s, c));
	printf("the total empty hole: %d\n", (int)offsetof(struct s, c)- (int)sizeof(test.b)- (int)sizeof(test.a));


}
