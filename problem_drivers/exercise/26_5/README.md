# 26_5 显示相差的天数 Numbers of days

## 题目

编写一个程序，提示用户录入两个日期，然后显示两个日期之间相差的天数。

## 样例

### 样例一

   Enter first date: Enter month (1-12): 2
   Enter day (1-31): 23
   Enter year: 2018
   Enter second date: Enter month (1-12): 4
   Enter day (1-31): 12
   Enter year: 2018

   The difference between two days is 48(Days)

### 样例二

   Enter first date: Enter month (1-12): 2
   Enter day (1-31): 23
   Enter year: 2004
   Enter second date: Enter month (1-12): 7
   Enter day (1-31): 11
   Enter year: 2006

   The difference between two days is 869(Days)

## 数据范围

日期要考虑到每个月的不同天数和是否为闰年的二月份天数。
按照规范的格式进行输入输出。

## 提示

使用 mktime 函数和 difftime 函数。
使用 time.h 头文件。