# 2_3 修改2_2.c Modify 2.2.c

## 题目

修改上题中的程序，使用户可以自行录入球体的半径。

## 样例

### 样例一

   Please enter the radius of the sphere：10
   4188.79

### 样例二

   Please enter the radius of the sphere：20
   33510.29

## 数据范围

可以假设球的半径是大于 0 的浮点数。
结果保留两位小数。
输入输入均在类型'float'数据范围内。
按照题目内容中的输出为规范格式进行输出。

## 提示

C语言没有指数运算符，所以需要对 r 自乘两次来计算 r3（ pi 为3.14159）。