# 8_4 修改 reverse.c Modify reverse.c

## 题目

修改 8.1 节的程序 reverse.c，利用表达式 `(int) (sizeof(a) / sizeof(a[0]))`（或者具有相同值的宏）来计算数组长度。

## 样例

### 样例一

    Enter 10 numbers: 23 44 23 45 2 565 1 9 3 44
    In reverse order: 44 3 9 1 565 2 45 23 44 23

### 样例二

    Enter 10 numbers: 1 2 3 4 5 6 7 8 9 10
    In reverse order: 10 9 8 7 6 5 4 3 2 1

## 数据范围

输入的数据为空格分割的十个 int 范围内的数据。