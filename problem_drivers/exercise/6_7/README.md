# 6_7 在for循环中初始化变量

## 题目

重新安排程序 square3.c，在 for 循环中对变量 i 进行初始化，判定以及自增操作。不需要重写程序，特别是不要使用任何乘法。

## 样例

### 样例一

    This program prints a table of squres.
    Enter a number of entries in table: 10
            1         1
            2         4
            3         9
            4        16
            5        25
            6        36
            7        49
            8        64
            9        81
            10       100

### 样例二

    This program prints a table of squres.
    Enter a number of entries in table: 5
            1         1
            2         4
            3         9
            4        16
            5        25

## 数据范围

设输入的数为 `n` ，则 n ≥ 1 ，且为整数。

## 提示

1. 按照题目内容中的输出为规范格式进行输出。