# 13_4 逆序输出命令行参数 Reverse arguments

## 题目

编写 reverse.c 的程序，用来逆序输出命令行参数。

## 样例

### 样例一

输入：

    reverse void and null

输出：

    null and void

### 样例二

输入：

    reverse hello everyone

输出：

    everyone hello

## 数据范围

命令行参数可以由任意字符组成。