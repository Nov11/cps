# 6_1 找出输入的最大的数

## 题目

编写程序，找出用户输入的一串数中的最大数。程序需要提示用户一个一个地输入数。当用户输入 0 或负数时，程序必须显示出已输入的最大非负数：

    Enter a number: 60
    Enter a number: 38.3
    Enter a number: 4.89
    Enter a number: 100.62
    Enter a number: 75.2295
    Enter a number: 0
    The largest number entered was 100.62

## 样例

### 样例一

    Enter a number: 61
    Enter a number: 654.1
    Enter a number: 6516.6
    Enter a number: 68461.651
    Enter a number: 315.516
    Enter a number: 021.651
    Enter a number: 0.01
    Enter a number: 0
    The largest number entered was: 68461.648

### 样例二

    Enter a number: 60
    Enter a number: 38.3
    Enter a number: 4.89
    Enter a number: 100.62
    Enter a number: 75.2295
    Enter a number: 0
    The largest number entered was 100.62

## 数据范围

设输入的数为 `n` ，则 n > 0

## 提示

1. 注意输入的数不一定是整数。

2. 最后输出需要保留3位小数

3. 按照题目内容中的输出为规范格式进行输出。