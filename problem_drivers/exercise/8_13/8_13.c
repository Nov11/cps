#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define BUFFERSIZE 40

int main(int argc, char *argv[])
{
	char buffer[BUFFERSIZE];
	memset(buffer, 0, BUFFERSIZE);
	int length;
	char name = 0;
	int index = 0;

	printf("Enter a first and last name: ");
	fgets(buffer, sizeof(buffer), stdin);
	length = strlen(buffer);
    for (; buffer[index] < 65; index++)
    	;
    name = buffer[index];
    for (; buffer[index] >= 65; index++)
    	;
    for (; buffer[index] < 65; index++)
    	;
    printf("You entered the name: ");
    for(;index < length - 1 && buffer[index] >= 65; index++)
    	printf("%c", buffer[index]);
    printf(", %c.\n", name);
    return 0;
}
