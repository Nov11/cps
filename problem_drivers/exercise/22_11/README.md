# 22_11 显示日期 Print date

## 题目

编写程序从命令行读取日期，并按照相应格式显示。

## 样例

### 样例一

    Enter a date (mm-dd-yyyy or mm/dd/yyyy): 9-13-2010
    September 13, 2010

### 样例二

    Enter a date (mm-dd-yyyy or mm/dd/yyyy): 6/24/2018
    June 24, 2018

### 样例三

    Enter a date (mm-dd-yyyy or mm/dd/yyyy): 6.24/2018
    Your date format is wrong.

### 样例四

    Enter a date (mm-dd-yyyy or mm/dd/yyyy): 13/24/2018
    Your date format is wrong.

## 数据范围

输入的日期中 mm 范围是 1-12, dd 范围是 1-31, yyyy 范围是 1-9999。分隔 mm、dd、yyyy的分隔符只能是 '-' 或 '/'。

## 提示

允许用户以 9-13-2010 或者 9/13/2010的形式录入日期，并假设日期中没有空格，如果没有按照指定格式录入日期，那么程序显示出错消息。
可以使用 sscanf 函数从命令行参数中提取月，日和年的信息。
