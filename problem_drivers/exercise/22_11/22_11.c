#include <stdio.h>
#include <stdlib.h>

#define DATE_SIZE 10
int main(void)
{
    char date[DATE_SIZE + 1];
    int month, day, year;
    printf("Enter a date (mm-dd-yyyy or mm/dd/yyyy): ");
    fgets(date, DATE_SIZE, stdin);
    int return_value = sscanf(date,"%d%*[-/]%d%*[-/]%d", &month, &day, &year);
    if(return_value < 3){
    	printf("Your date format is wrong.\n");
    	exit(1);
    }
    
    if(month > 12 || month < 1 || day > 31 || day < 1 ||
       year > 9999 || year < 1) {
    	printf("Your date format is wrong.\n");
    	exit(1);   
    }
    
    if(month==1){
        printf("January %d,%d\n", day, year);
	} 
	else if(month==2){
		printf("February %d,%d\n", day, year);
	}
	else if(month==3){
		printf("March %d,%d\n", day, year);
	}
	else if(month==4){
		printf("April %d,%d\n", day, year);
	}
	else if(month==5){
		printf("May %d,%d\n", day, year);
	}
	else if(month==6){
		printf("June %d,%d\n", day, year);
	}
	else if(month==7){
		printf("July %d,%d\n", day, year);
	}
	else if(month==8){
		printf("August %d,%d\n", day, year);
	}
	else if(month==9){
		printf("September %d,%d\n", day, year);
	}
	else if(month==10){
		printf("October %d,%d\n", day, year);
	}
	else if(month==11){
		printf("November  %d,%d\n", day, year);
	}
	else if(month==12){
		printf("December %d,%d\n", day, year);
	}
	
    return 0;
}
