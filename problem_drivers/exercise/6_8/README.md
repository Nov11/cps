# 6_8 打印日历

## 题目

编写程序显示单月的日历。用户指定这个月的天数和该月起始日是星期几：

    Enter number of days in month: 31
    Enter starting day of the week(1=Sun，7=Sat): 3

           1  2  3  4  5  
     6  7  8  9 10 11 12
    13 14 15 16 17 18 19  
    20 21 22 23 24 25 26
    27 28 29 30 31

## 样例

### 样例一

    Enter number of days in month: 31
    Enter starting day of the week(1=Sun，7=Sat): 3

    Enter number of days in month: 31
    Enter starting day of the week(1=Sun，7=Sat): 3

           1  2  3  4  5  
     6  7  8  9 10 11 12
    13 14 15 16 17 18 19  
    20 21 22 23 24 25 26
    27 28 29 30 31

### 样例二

    Enter number of days in month: 31
    Enter starting day of the week (1=Sun, 7=Sat): 5
                  1  2  3
      4  5  6  7  8  9 10
     11 12 13 14 15 16 17
     18 19 20 21 22 23 24
     25 26 27 28 29 30 31


## 数据范围

设输入的天数为`n`，星期数为`m`， 则 n ∈ {28， 29， 30， 31}， 1 ≤ m ≤ 7 且 m 为整数

## 提示

1. 此程序不像看上去那么难，最重要的部分是一个使用变量 i 从 1 计数到 n 的 for 语句（这里 n 是此月的天数），for 语句中需要显示 i 的每一个值。在循环中，用 if 语句判定 i 是否是一个星期的最后一天，如果是，就显示一个换行符。

2. 按照题目内容中的输出为规范格式进行输出。