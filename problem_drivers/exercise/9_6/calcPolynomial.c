#include <math.h>

double count_polynomial(double x){
	return 3*pow(x, 5) + 2*pow(x, 4) - 5*pow(x, 3) - x*x + 7*x + 6;
}