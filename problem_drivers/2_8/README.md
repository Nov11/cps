# 2_8 显示日期 Output date

## 题目

编写程序显示单月的日历。用户指定这个月的天数和该月起始日是星期几.

## 样例

### 样例一

      Enter number of days in month：31
      Enter starting day of the week(1=Sun, 7=Sat): 3
             1  2  3  4  5
       6  7  8  9 10 11 12
      13 14 15 16 17 18 19
      20 21 22 23 24 25 26
      27 28 29 30 31

### 样例二

      Enter number of days in month：30
      Enter starting day of the week(1=Sun, 7=Sat): 4
                1  2  3  4
       5  6  7  8  9 10 11
      12 13 14 15 16 17 18
      19 20 21 22 23 24 25
      26 27 28 29 30

## 数据范围

输入均属于正常范围，天数为 29~31 内的整数，开始日期为 1~7 内的整数。