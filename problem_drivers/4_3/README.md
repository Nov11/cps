# 4_3 逆序打印 Reversal

## 题目

编写程序读一条消息,然后逆序打印出这条消息。

## 样例

### 样例一

    Enter a message: Don't get mad, get even.
    Reversal is: .neve teg ,dam teg t'noD

### 样例二

    Enter a message: hello,world.
    Reversal is: .dlrow,olleh

## 数据范围

输入数据为回车符结尾的字符串。字符长度不超过 50 位。

## 提示

一次读取消息中的一个字符(用`getchar`函数),并且把这些字符存储在数组中,当数组满了或者读到字符`'\n'`时停止读操作。
