# 逆序打印(Reversal)
## 题目编号 : 12-1
## 题目名称 :
编写程序读一条消息,然后逆序打印出这条消息。
## 输出范例一 : 

    Enter a message: Don't get mad, get even.
    Reversal is: .neve teg ,dam teg t'noD

## 输出范例二 : 

    Enter a message: hello,world.
    Reversal is: .dlrow,olleh

## 数据范围

输入数据为回车符结尾的字符串。字符长度不超过 50 位。

## 提示

一次读取消息中的一个字符(用`getchar`函数),并且把这些字符存储在数组中,当数组满了或者读到字符`'\n'`时停止读操作。


## 测试用例

hello world.\\n
 123456 789\\n
abcde\\n
Lorem ipsum dolor sit amet, consectetur adipisici\\n
 miaomiaomiao\\n
45 + 34556 = 345 #$%^&*~!@#.\\n
sdfgre2345\\n
wedfjgh\\n
13345567\\n
skd jf.\\n
yes i do.\\n
