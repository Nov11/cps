# 1_2 最少钞票 Bills calculate

## 题目

编写一个程序，要求用户输出一个美元数量，然后显示出如何用最少20美元、10美元、5美元和1美元来付款。

## 样例

### 样例一

    Enter a dollar amount: 93
    $20 bills: 4
    $10 bills: 1
    $5  bills: 0
    $1  bills: 3

### 样例二

    Enter a dollar amount: 156
    $20 bills: 7
    $10 bills: 1
    $5 bills: 1
    $1 bills: 1

## 数据范围

输入数据为 int 范围内的正整数。