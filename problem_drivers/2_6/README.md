# 2_6 计算股经纪人的佣金 Calculate commission stock broker

## 题目

在5.2节的broker.c程序中添加循环，以便用户可以输入多笔交易并且程序可以计算每次的佣金。程序在用户输入的交易额为0时终止。

## 样例

### 样例一

    Enter value of trade：30000
    Commission：$166.00

    Enter value of trade：20000
    Commission：$144.00

    Enter value of trade：0

### 样例二

    Enter value of trade：526
    Commission：$39.00

    Enter value of trade：0

## 数据范围

输入的交易额为大于 0 的小数。输出保留两位小数的佣金。