# 1_4 格式化日期 Date format

## 题目

编写一个程序，以月/日/年的格式接受用户录入的日期信息，并以年月日的格式将其显示出来。

## 样例

### 样例一

    Enter a date (mm/dd/yyyy): 2/17/2011
    You entered the date: 20110217

### 样例二

    Enter a date(mm/dd/yyyy): 4/5/2003
    You entered the date: 20030405

## 数据范围

month 范围为 1 - 12；day 范围为1 - 31。year 为四位数字。