#include<stdio.h>
#include<string.h>

#define MAX_SIZE 20

int main(){
    char smallest_word[MAX_SIZE];
    char largest_word[MAX_SIZE];
    char init_word[2][MAX_SIZE];
    char temp[MAX_SIZE];
    for(int i = 0; i < 2; i++){
        printf("Enter word: ");
        /*get_word(init_word[i]);*/
        scanf("%s",init_word[i]);
    }
    if(strcmp(init_word[0],init_word[1]) > 0){
        strcpy(smallest_word,init_word[1]);
        strcpy(largest_word,init_word[0]);
    }
    else{
        strcpy(smallest_word,init_word[0]);
        strcpy(largest_word,init_word[1]);
    }
    printf("Enter word: ");
    while(1){
        scanf("%s",temp);
        if(strlen(temp) == 4)
            break;
        printf("Enter word: ");
        if(strcmp(smallest_word,temp) > 0){
            strcpy(smallest_word,temp);
        }
        if(strcmp(largest_word,temp) < 0){
            strcpy(largest_word,temp);
        }
    }
    printf("\nSmallest word: %s\n",smallest_word);
    printf("Largest word: %s\n",largest_word);
    return 0;
}

