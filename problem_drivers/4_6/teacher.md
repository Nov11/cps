# 找单词(Find biggest and smallest word)
## 题目编号 : 13-1
## 题目名称 : 
 实验项目4.6 编写程序找出一组单词中“最小”单词和“最大”单词。用户输入单词后,程序根据字典顺序决定排在最前面和最后面的单词。当用户输入4个字母的单词时,程序停止读入。假设所有单词都不超过20个字母。
## 输出范例一 : 

    Enter word: dog
    Enter word: zebra
    Enter word: rabbit
    Enter word: catfish
    Enter word: walrus
    Enter word: cat
    Enter word: fish

    Smallest word: cat
    Largest word zebra

## 输出范例二 : 

    Enter word: computer
    Enter word: ink
    Enter word: apple
    Enter word: banana
    Enter word: milkshake
    Enter word: desk

    Smallest word: apple
    Largest word: milkshake

## 数据范围

至少输入3个单词，最后一个单词长度为4个字母。每个单词长度不超过 20 位。

## 提示

使用两个名为`smallest_word`和`largest_word`的字符串来分别记录所有输入中的“最小”单词和“最大”单词。用户每输入一个新单词,都要用 `strcmp`函数把它与`smallest_word`进行比较如果新的单词比`sma11est_word`“小”,就用 `strcpy`函数把新单词保存到`sma11est_word`中。用类似的方式与`largest_word`进行比较。用`str1en`函数来判断用户是否输入了4个字母的单词。


## 测试用例

dog\\nzebra\\nrabbit\\ncatfish\\nwalrus\\ncat\\nfish\\n
baa\\nvsssss\\nyaaa\\naaa\\nyyy\\naa\\niiii\\n
aazaa\\nbbb\\nvvvvvv\\nvvvvva\\nzzzza\\nzzzzb\\naabaa\\nabcd\\n
yes\\nabc\\nabcdefglesge\\naaaaa\\nbus\\nzzzza\\nabcd\\n
largets\\nsmallest\\napple\\nbanana\\npaper\\nfish\\n
computer\\nink\\nkeyboard\\nmusic\\nabuntun\\napen\\n
ammms\\nkjetu\\nabcd\\n
zages\\nkwert\\nabcd\\n
big\\nbanana\\njuice\\ndesk\\n
computer\\nink\\nlight\\nheavy\\nmilk\\n
