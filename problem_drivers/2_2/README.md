# 2_2 显示对风速的描述 Wind Speed

## 题目

下面是用于测量风力的蒲福风力等级的简化版。

速率（海里/小时）| 描述
-----------------|-----
小于 1 | Calm（无风）
1~3 | Light air（轻风）
4~27 | Breeze（微风）
28~47 | Gale（大风）
48~63 | Storm（暴风）
大于63 | Hurricane（飓风）

编写一个程序，要求用户输入风速（海里/小时），然后显示相应的描述。

## 样例

### 样例一

    Enter a wind speed： 1
    Light air

### 样例二

    Enter a wind speed： 27
    Breeze

## 数据范围

输入的风速为大于等于 0 的整数。
